const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
const total = document.getElementById('total');

populateUI();

let ticketPrice = 40000;

// Menyimpan harga
function setMovieData(moviePrice) {
  localStorage.setItem(moviePrice);
}

// Update total dan jumlah tiket
function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  const seatsIndex = [...selectedSeats].map(seat => [...seats].indexOf(seat));

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  const selectedSeatsCount = selectedSeats.length;

  if (selectedSeatsCount == 0) {
    count.innerText = selectedSeatsCount;
    total.innerText = 0;
  } else{
  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice + ( 1000 * selectedSeatsCount);
  }
  setMovieData(ticketPrice);
}

// mengambimbil data dari local stroage dan populate ui
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));

  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add('occupied');
      }
    });
  }
}

// Seat click event
container.addEventListener('click', e => {
  if (
    e.target.classList.contains('seat') &&
    !e.target.classList.contains('occupied')
  ) {
    e.target.classList.toggle('selected');

    updateSelectedCount();
  }
});

function saveSeat() {
  alert("Data Berhasil disimpan !");
 window.location.reload();
 updateSelectedCount();
}

// Initial count and total set
updateSelectedCount();